(use-package emacs
  :init
  (recentf-mode 1)
  (delete-selection-mode 1)
  (electric-indent-mode 1)
  (electric-pair-mode 1)
  (global-visual-line-mode t)
  (global-hl-line-mode 1)
  (global-display-line-numbers-mode 1)
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (add-to-list 'custom-theme-load-path "~/.config/emacs/themes")
  (load-theme 'ujelly t)
  (add-to-list 'default-frame-alist '(font . "JetBrainsMono Nerd Font-12"))
  (set-face-attribute 'default nil
                      :font "JetBrainsMono Nerd Font"
                      :height 120)
  (set-face-attribute 'variable-pitch nil
                      :font "JetBrainsMono Nerd Font"
                      :height 130
                      :weight 'medium)
  (set-face-attribute 'fixed-pitch nil
                      :font "JetBrainsMono Nerd Font"
                      :height 120
                      :weight 'medium)
  (set-face-attribute 'font-lock-comment-face nil
                      :slant 'italic)
  (set-face-attribute 'font-lock-keyword-face nil
                      :slant 'italic)
  (setq-default line-spacing 0.15)
  (add-to-list 'package-archives
               '("melpa" . "https://melpa.org/packages/"))
  :custom
  (custom-file "~/.config/emacs/custom.el")
  :config
  (load custom-file)
  :bind
  ("C-=" . 'text-scale-increase)
  ("C--" . 'text-scale-decrease)
  ("<C-wheel-up>" . 'text-scale-increase)
  ("<C-wheel-down>" . 'text-scale-decrease)
  ([escape] . 'keyboard-escape-quit))

(use-package files
  :custom
  (auto-save-file-name-transforms '((".*" "~/.config/emacs/autosaves/\\1" t)))
  (backup-directory-alist '((".*" . "~/.config/emacs/backups/"))))

(use-package company
  :ensure t
  :init
  (global-company-mode)
  :custom
  ((company-idle-delay 0.1)
   (company-minimum-prefix-length 2))
  ;; :hook
  ;; (prog-mode . company-mode)
  :bind (:map company-active-map
	      ("TAB" . company-indent-or-complete-common)
              ("C-j" . company-select-next)
              ("C-k" . company-select-previous)))

(use-package company-box
  :ensure t
  :hook (company-mode . company-box-mode))

(use-package dashboard
  :ensure t
  :custom
  (dashboard-banner-logo-title "What's your ratio of configuring emacs to actually coding in emacs")
  (dashboard-startup-banner "~/.config/emacs/themes/levitating-gnu.png")
  (dashboard-show-shortcut t)
  (dashboard-center-content t)
  (dashboard-vertically-center-content t)
  (dashboard-items '((recents . 5) (agenda . 5) (projects . 5)))
  :config
  (dashboard-setup-startup-hook))

(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1))

(use-package evil
  :ensure t
  :custom
  (evil-undo-system 'undo-redo)
  :bind
  (:map evil-motion-state-map
	("SPC" . nil)
	("RET" . nil)
	("TAB" . nil))
  :config
  (evil-mode 1))

(use-package general
  :ensure t
  :config
  (general-create-definer my-leader
			  :states '(normal insert visual emacs)
			  :keymaps 'override
			  :prefix "SPC"
			  :global-prefix "M-SPC")
  (my-leader
   :states 'normal
   "SPC" '(execute-extended-command :wk "Execute Command")
   "RET" '(consult-bookmark :wk "Set a bookmark")
   "h t" '(consult-theme :wk "Load Theme"))

(my-leader
  "b" '(:ignore t :wk "Bookmarks/Buffers")
  "b b" '(consult-buffer :wk "Switch to buffer")
  "b k" '(kill-current-buffer :wk "Kill Current Buffer")
  "b n" '(next-buffer :wk "Next Buffer")
  "b p" '(previous-buffer :wk "Previous Buffer")
  "b m" '(make-frame :wk "Send window to new frame"))

(my-leader
  "f" '(:ignore t :wk "Files")
  "f f" '(find-file :wk "Find File")
  "f r" '(consult-recent-file :wk "Recent Files")
  "f l" '(load-file :wk "Load file"))

(my-leader
  "w" '(:ignore t :wk "Manipulate windows")
  "w k" '(evil-window-up :wk "Move to the window above")
  "w j" '(evil-window-down :wk "Move to the window below")
  "w h" '(evil-window-left :wk "Move to the window left")
  "w l" '(evil-window-right :wk "Move to the window right")
  "w s" '(evil-window-split :wk "Horizontally split window")
  "w v" '(evil-window-vsplit :wk "Vertically split window")
  "w c" '(evil-window-delete :wk "Close current window")))

(use-package org
  :custom
  (org-indent-mode-turns-off-org-adapt-indentation nil)
  (org-edit-src-content-indentation 0)
  (org-return-follows-link t)
  (org-confirm-babel-evaluate nil)
  :hook
  (org-mode . org-indent-mode)
  :config
  (add-to-list 'org-src-lang-modes '("python" . "python"))
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((python . t)
     (shell . t))))

(use-package org-tempo
  :hook
  (org-mode . (lambda ()
                (setq-local electric-pair-inhibit-predicate
                            `(lambda (c)
                               (if (char-equal c ?<) t (,electric-pair-inhibit-predicate c)))))))

(use-package org-agenda
  :custom
  (org-agenda-start-on-weekday -1)
  (org-agenda-span 14)
  (org-agenda-start-day "4d")
  (org-agenda-start-with-log-mode t)
  (org-log-done 'time)
  (org-log-into-drawer t)
  :general
  (:states 'normal
	   "SPC oa" 'org-agenda))

(use-package org-bullets
  :ensure t
  :hook
  (org-mode . org-bullets-mode))

(use-package toc-org
  :ensure t
  :hook
  (org-mode . toc-org-mode))

(use-package direnv
  :ensure t
  :config
  (direnv-mode))

(use-package eglot
  :ensure t
  :hook
  ((python-mode . eglot-ensure))
  :custom
  (eglot-report-progress nil)
  :config
  (fset #'jsonrpc--log-event #'ignore))

(use-package eldoc-box
  :ensure t
  :hook
  (eglot-managed-mode . eldoc-box-hover-mode)
  :config
  (add-to-list 'eglot-ignored-server-capabilites :hoverProvider))

(use-package nix-mode
  :ensure t
  :defer t
  :mode ("\\.nix\\'" "\\.nix.in\\'"))

(use-package vertico
  :ensure t
  :bind
  (:map vertico-map
  	("RET" .  #'vertico-directory-enter)
  	("DEL" .  #'vertico-directory-delete-char)
  	("M-DEL" . #'vertico-directory-delete-word)
  	("C-k" . #'vertico-previous)
  	("C-j" . #'vertico-next))
  :init
  (vertico-mode 1)
  :hook
  (rfn-eshadow-update-overlay . vertico-directory-tidy)
  :custom
  (vertico-cycle t)
  (vertico-resize t))

(use-package consult
  :ensure t
  :after
  (vertico))

(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-defaults nil)
  (completion-category-overrides '((file (styles partial-completion)))))

(use-package marginalia
  :ensure t
  :init
  (marginalia-mode))

(use-package which-key
  :ensure t
  :init
  (which-key-mode 1)
  :diminish
  :custom
  (which-key-side-window-location 'bottom)
  (which-key-sort-order #'which-key-key-order-alpha)
  (which-key-allow-imprecise-window-fit nil)
  (which-key-sort-uppercase-first nil)
  (which-key-add-column-padding 1)
  (which-key-max-display-columns nil)
  (which-key-min-display-lines 6)
  (which-key-side-window-slot -10)
  (which-key-side-window-max-height 0.25)
  (which-key-idle-delay 0.8)
  (which-key-max-description-length 25)
  (which-key-allow-imprecise-window-fit nil)
  (which-key-separator " → " ))
